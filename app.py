import os, cx_Oracle
from flask import Flask
from flask import Response
from flask import json

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello World!"


@app.route('/db')
def get_message():
  os.environ['TNS_ADMIN'] = '/home/opc'
  con = cx_Oracle.connect(user = 'dbadmin',password = 'Aut0nomousD4mo',dsn = 'atpdb_tp')
  cursor = con.cursor()
  cursor.execute("""SELECT MSG FROM MESSAGES WHERE ID = 1""")
  res = cursor.fetchone()[0]
  cursor.close()
  con.close()
  return res

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)


