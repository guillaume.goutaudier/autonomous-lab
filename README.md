# Introduction
The objective of this lab is to illustrate how it is possible to use the new Oracle Cloud Free Tier to to create a well-protected web application that stores its data in an Autonomous database.

The lab should take approximatively 2 hours to complete. In the end of the lab, we will have created the following architecture:
![Archi.png](Architecture/Archi.png)


# Oracle Cloud Free Tier account setup
Just go to http://cloud.oracle.com and click on "Oracle Cloud Free Tier" on the top right of the page. 

Then click on "Start for free" and complete the registration procedure. You will need a valid email address, mobile numnber, and credit card.

To connect to your newly created account, you will be able to use a sign-in URL like this one:
https://myservices-<your_tenancy_name>.console.oraclecloud.com

If you do not remember this URL, you can always connect from cloud.oracle.com.


# Setup a Compartment in your tenancy
To isolate the work we will be doing in this workshop from the rest of your environment, it is good practice to create a dedicated compartment. 

To do that, go to `Identity > Compartments`. You will see a root compartment that contains all the services you are entitled to you on this tenancy and their associated limits. 

Click on the root compartment, then create a Child comparment with the name of your choice, e.g. `WebApp`. By default no quotas will be imposed on this Child Compartment so you will be able to use all the resources available from the parent compartment.

For the rest of the lab, please create all resources in this compartment. Note that specific IAM roles with access limited to this compartment could also be setup, but this is beyond the scope of this lab.

# Create a VCN
Go to `Networking > Virtual Cloud Networks` and click on `Networking Quickstart`. Select `VCN with Internet Connectivity` and use the following parameters:
- VCN name: 'VCN' 
- VCN CIDR range: 10.0.0.0/16
- Public subnet CIDR 10.0.0.0/24
- Private subnet CIDR 10.0.1.0/24
- Keep all other default values


# Create an Autonomous Database
There are 2 types of autonomous databasses: Autonomous Data Warehouse and Autonomous Transaction Processing. They are designed for different types of data workloads. In this lab we will use an Autonomous Transaction Processing database but one could use an Autonomous Data Warehouse instead.

Go to `Autonomous Transaction Processing` and click `Create Autonomous Database`. Use the following parameters:
- Name: 'ATPDB'
- ADMIN password: 'Aut0nomousD4mo'
- Click `Configure access control rules` and select the VCN previously created


# Create Bastion host
Now go to `Compute > Instances` and click `Create Instance`. Use the following parameters:
- Name: "bastion"
- Image: "Oracle Linux 7.7"
- Shape: "VM.Standard2.1"

Wait for the instance to be created, then connect to it via SSH using its public IP address. Note that you could use Cloud Shell for that. When connected, validate you can execute some basic adminitrative tasks: 
```
ssh -o ServerAliveInterval=30 opc@<instance_public_ip>
[opc@bastion ~]$ echo -e "LANG=en_US.utf-8\nLC_ALL=en_US.utf-8" | sudo tee -a /etc/environment
[opc@bastion ~]$ ssh-keygen 
[opc@bastion ~]$ cat .ssh/id_rsa.pub
```
Keep the output of the last command for the next section.

# Create the web server
Create a new instance with the following parameters:
- Name: webserver
- Image: Oracle Linux 7.7
- Shape: "VM.Standard2.1"
- Subnet: private-subnet (note this will prevent the assignment of a public address)
- SSH Key: SSH key generated on the Bastion host (see output from previous section)
- in the Advanced Options, paste this cloud-init script: 
```
#!/bin/bash
sudo yum install -y python3
sudo pip3 install cx_Oracle
sudo pip3 install pandas
sudo pip3 install flask
sudo yum install -y oracle-release-el7
sudo yum-config-manager --enable ol7_oracle_instantclient
sudo yum install -y oracle-instantclient19.3-basic
sudo systemctl stop firewalld.service
sudo systemctl disable firewalld.service
```

Click Create. Wait for the instance to start, grab its private IP and connect to it from the Bastion host:
```
[opc@bastion ~]$ ssh -o ServerAliveInterval=30 <instance_private_ip>
```

It is interesting to note here that we could connect to the webserver without adding any route. This is because by default all subnets from a given VCN are automatically routed to each other.

# Create new DB user
Wait for the database to be provisioned (it only takes a couple of minutes), and click on `Service Console`. We will use the integrated SQL Developer Web tool to create a DB user.

Go to `Service Console > Development > SQL Developer Web`. Use the ADMIN username and Aut0nomousD4mo password to sign in. Then execute the following SQL commands in the Worksheet:
```
CREATE USER dbadmin IDENTIFIED BY Aut0nomousD4mo;
GRANT CREATE SESSION TO dbadmin;
GRANT DWROLE TO dbadmin;
GRANT UNLIMITED TABLESPACE TO dbadmin;
```

You can also show the number of CPUs using this command:
```
SELECT * FROM V$PARAMETER WHERE NAME='cpu_count'
```


# Create webserver

To connect to the database from the web server, we need to use the "wallet" that has been automatically created with the Autonomous Database. So just go to `Autonomous Transaction Processing`, click on the `ATPDB` database, then on `DB Connection`, and then on `Download Wallet`. Enter `Aut0nomousD4mo`as password.

You then need to transfer the wallet to the web server using SCP via the Bastion host (below commands must be adapted to your environment):
```
scp Wallet_ATPDB.zip opc@<bastion_public_ip>:
ssh -o ServerAliveInterval=30 opc@<bastion_public_ip>
[opc@bastion ~]$ scp Wallet_ATPDB.zip opc@<webserver_private_ip>:
[opc@bastion ~]$ ssh -o ServerAliveInterval=30 opc@<webserver_private_ip>
[opc@webserver ~]$ unzip Wallet_ATPDB.zip 
```

Now edit `sqlnet.ora`and replace the DIRECTORY string with `"/home/opc"` 

We also need to choose the connection string we want to use (they are listed in the tnsnames.ora file). Based on the name of the database that we chose We should be able to use a connection named `atpdb_tp`.

That's it! We should now be able to connect to the database and create a table with some data in it:
```python
[opc@webserver ~]$ python3
>>> import os, cx_Oracle 
>>> os.environ['TNS_ADMIN'] = '/home/opc'
>>> con = cx_Oracle.connect(user = 'dbadmin',password = 'Aut0nomousD4mo',dsn = 'atpdb_tp')
>>> cursor = con.cursor()
>>> cursor.execute('CREATE TABLE MESSAGES ( ID NUMBER(32) , MSG VARCHAR2(32))')
>>> cursor.execute("""INSERT INTO MESSAGES (ID, MSG) VALUES(1,'AUTONOMOUS DEMO')""")
>>> cursor.execute("""SELECT MSG FROM MESSAGES WHERE ID = 1""")
>>> print(cursor.fetchone()[0])
>>> cursor.execute('COMMIT')
>>> con.close()
>>> exit()
```

Now let's put some load on the database and observe the behaviour.
You can use the `generate_activity.py` script to do that (enter this command as many times as you want to consume vCPUs):

```
[opc@webserver ~]$ python3 generate_activity.py &
[opc@webserver ~]$ python3 generate_activity.py &
[opc@webserver ~]$ python3 generate_activity.py &
```

Select `ATPDB` and click on `Performance Hub`. You should see a graph similar to this one:
![load.png](./load.png)

# Create a Web App and make it accessible from a Load Balancer
The web application is contained in the `app.py` file. It contains a very simple Flask server that listens on port 5000. The server returns "Hello World!" when queried on the "/" path and dynamically connects to the Autonomous database and retrives the first raw of the MESSAGES table when queried on the "/db" path. 

Similarly to what you did with the wallet, copy the application to the web server.

Then start the web application and check it works properly:
```
[opc@webserver ~]$ python3 app.y
[opc@webserver ~]$ curl localhost:5000
[opc@webserver ~]$ curl localhost:5000/db
# Keep the web application running in this terminal and start a new one for the next steps
```

Let's now create a Load Balancer to make the web server accessible from the Internet. The load balancer will be created on the public subnet. It will listen on port 80 and dynamically query the web server on the private subnet on port 5000 when it receives HTTP requests. For this to work we need to modify the security list of the VCN to allow the corresponding flows. 

To allow incoming queries on port 80 on the public subnet, edit the `Default Security List for VCN` and add the following Ingress rules:
- Source: 0.0.0.0/0, Protocol: TCP, Destination Port: 80

To allow incoming queries on port 5000 on the private subnet, edit the `Default Security List for VCN` and add the following Ingress rules:
- Source: 10.0.0.0/24, Protocol: TCP, Destination Port: 5000

Before creating the load balancer, let's check that we can query the web server from the Bastion host:
```
[opc@bastion ~]$ curl <webserver_private_ip>:5000
[opc@bastion ~]$ curl <webserver_private_ip>:5000/db
```

# [Optional] Prove that Autonomous Database is accessed privately
Edit the route table of the Private Subnet, and remove temporarily the route to the NAT Gateway.
You will see that the webserver is not able to reach the Internet anymore, but that it can still query the ATPDB.


# Create Load Balancer
To create the load balancer, go to `Networking > Load Balancers` and click `Create Load Balancer`. Use the following parameters:
- Name: public-lb
- Visibility: Public
- Total Bandwidth: Small
- Subnet: Public subnet from VCN
- Load balancing policy: Weighted Round Robin
- Backend: webserver with Port 5000 (default port is 80, do not forget to change it)
- Health check policy: HTTP 5000 every 10 seconds
- Listener: HTTP port 80

Wait for the Load Balancer to be created, and check the health of the backend. You should see some incoming request on the web server and the Overall Health of the Load Balancer should change to `OK`.

Grap the public IP address of the Load Balancer and connect to it in a browser. You should see the "Hello World!" message. Try adding "/db" to the URL and you should see the "AUTONOMOUS DEMO" message.

CONGRATULATIONS! You have just created a complete web application leveraging an autonomous database and important Oracle Cloud capabilities. And it did not cost you anything. Well done!

# Clean-up
To clean-up our environment, we need to:
- Terminate the Load Balancer
- Terminate the 2 instances (select the "Permanently delete the attached Boot Volume" option when you do so)
- Terminate the Autonomous Database
- Terminate the VCN (this will terminate all the network resources contained in the VCN as well)
- Delete the WebApp compartment (you will not be able to delete it immediately)

# References
https://github.com/Carl-Lejerskar/DatawareHouse_Connection_Python





