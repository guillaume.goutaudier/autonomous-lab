import os, cx_Oracle
os.environ['TNS_ADMIN'] = '/home/opc'
con = cx_Oracle.connect(user = 'dbadmin',password = 'Aut0nomousD4mo',dsn = 'atpdb_tp')
cursor = con.cursor()
cursor.callproc("dbms_output.enable")
#curs.callproc("dbms_output.enable", (None,))
 
statement="""
declare
            l_number    number;
            l_number2   number;
            l_start_time number := dbms_utility.get_time();
    begin
            for i in 1 .. 90000000
            loop
                    if mod(i,2) = 0 then l_number2 := 42; else l_number2 := 55; end if;
                    l_number := ln(l_number2);
           end loop;
           dbms_output.put_line( (dbms_utility.get_time-l_start_time) || ' elapsed hsecs' );
   end;
"""
cursor.execute(statement)
 
# Get back elapsed time
# hsecs : number of hundredths of a second of processing time
# lineVar = curs.var(cx_Oracle.STRING)
# statusVar = curs.var(cx_Oracle.NUMBER)
lineVar = cursor.var(str)
statusVar = cursor.var(int)
while True:
  cursor.callproc("dbms_output.get_line", (lineVar, statusVar))
  if statusVar.getvalue() != 0:
    break
  print(lineVar.getvalue())
 
con.close()
